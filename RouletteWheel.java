import java.util.Random;
public class RouletteWheel 
{
    private Random spin;
    private int number;

    public RouletteWheel() 
    {
        this.spin = new Random();
        this.number = 0;
    }
    public void spin() 
    {
        this.number = this.spin.nextInt(37);
    }
    public int getValue() 
    {
        return this.number;
    }
}
