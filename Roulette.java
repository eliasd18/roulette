import java.util.Scanner;
public class Roulette
{
public static void main(String[] args) 
{
    Scanner reader = new Scanner(System.in);
    RouletteWheel wheel = new RouletteWheel();
    int money = 1000; // Initializing object, user's money and money they win from the game
    int moneyWon = 0;

    System.out.println("Welcome to Roulette!");

    System.out.println("Would you like to make a bet? Press 1 if yes, anything else if no");
        int makeBet = reader.nextInt();
    
    if (makeBet == 1) {
    System.out.println("What number would you like to bet on?");
        int number = reader.nextInt();

    while (number > 36 || number < 0) 
    {
        System.out.println("Try again"); // check if number can be used
        number = reader.nextInt();
    }

    System.out.println("How much money will you bet?");
        int bet = reader.nextInt();

    while (bet > 1000 || bet < 0) 
    {
        System.out.println("Try again"); // check if money is between 0 and 1000
        bet = reader.nextInt();
    }

    money = money - bet;   // substract user's balance
    System.out.println("Spinning the wheel...");                 
    wheel.spin();                               // spinning the wheel with method from RouletteWheel class
    System.out.println("Got " + wheel.getValue());
    Boolean hasWon = checkIfWon(number, wheel); // check if user  won

    if (hasWon != false) 
    {
        moneyWon = number*35;
        money = money + moneyWon; // update user's balance and tell them how much they won
        System.out.println("You were right!"); 
        System.out.println("You've won " + moneyWon);
        System.out.println("Updated balance: " + money);
    }
    else 
    {
        System.out.println("You lost!"); 
        System.out.println("Updated balance: " + money);
    }
    }
}
public static boolean checkIfWon(int number, RouletteWheel roulette) // helper method to check if user won the spin
{
    int rouletteNumber = roulette.getValue();

    if (number == rouletteNumber) 
    {
        return true;
    }
        return false;
}
}